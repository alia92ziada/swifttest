//
//  ViewController.swift
//  test2
//
//  Created by Aliaa Zyada on 12/13/15.
//  Copyright © 2015 Ntime. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func hideFirstImg(sender: AnyObject) {
        img1.hidden=true;
    }

    @IBAction func hideSecondImg(sender: AnyObject) {
        img2.hidden=true;
    }
}

